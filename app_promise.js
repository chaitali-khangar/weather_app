const yargs = require('yargs');
const geocode = require('./geocode/geocode');
const weather = require('./geocode/weather');

const args = yargs
  .options({
    address:{
      describe: 'Address to fetch weather',
      demand: true,
      alias: 'a',
      string: true
    }
  })
  .help()
  .alias('help','h')
  .argv;
// geocode.geocodeAddress(args.address,(errorMessage,result) => {
//   if(errorMessage){
//     console.log(errorMessage);
//   }else{
//     console.log(`Address: ${result.address}`);
//     weather.weather_report(result.latitude,result.longitude,(errorMessage,weatherResult) => {
//       if(errorMessage){
//         console.log(errorMessage);
//       }else{
//         console.log(`It's currently ${weatherResult.temperature}. It's feel like ${weatherResult.apparentTemperature}`);
//       }
//     });
//   }
// });

geocode.geocodeAddress(args.address)
  .then((location) => {
    console.log(`Address: ${location.address}`);
    return weather.weather_report(location.latitude,location.longitude);
  })
  .then((weatherResult) => {
    console.log(`It's currently ${weatherResult.temperature}. It's feel like ${weatherResult.apparentTemperature}`);
  })
  .catch((errorMessage) => {
    console.log(errorMessage);
  })
