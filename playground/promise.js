var asyncAdd = (a,b) =>{
  return new Promise((resolve,reject) => {
    setTimeout(() => {
      if(typeof a === 'number' && typeof b === 'number'){
        resolve(a+b);
      }else{
        reject('Argument should be number');
      }
    },1500);
  })
}

asyncAdd(3,'hi').then((result) =>{
  console.log(`Result is ${result}`);
  return asyncAdd(result, 4);
}).then((result) => {
  console.log(`Result is ${result}`);
}).catch((error) =>{
  console.log(error);
});
//
// var somePromise = new Promise((resolve,reject) =>{
//   setTimeout(() => {
//    // resolve('hey it worked!');
//    reject('Unable to fulfill promise');
//   },2500);
//
// });
// somePromise.then((message)=>{
//   console.log(message);
// }, (errorMessage) => {
//   console.log(errorMessage);
// })
