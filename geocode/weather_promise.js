const request = require('request');
const _ = require('lodash');
API_KEY = "ecf3d435c018a50b09ef3769f693023c"
var weather_report = (latitude,longitude, callback) => {
  return new Promise((resolve,reject) => {
    request({
      url: `https://api.darksky.net/forecast/${API_KEY}/${latitude},${longitude}`,
      json: true
    },(error,response,body) =>{
      if (!error && response.statusCode === 200){
        resolve(
          {
            temperature: body.currently.temperature,
            apparentTemperature: body.currently.apparentTemperature
          }
        );
      }else{
        reject('Unable to fetch weather');
      }
    });
  });
}
module.exports.weather_report = weather_report;
