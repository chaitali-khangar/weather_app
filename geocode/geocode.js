const request = require('request');
var geocodeAddress = (address, callback) => {
  var encodedAddress = encodeURI(address);
  return new Promise((resolve,reject) => {
    request({
      url: `http://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`,
      json: true
    },(error,response, body) =>{
      if(error){
        reject('Unable to connect to google Server');
      }else if(body.status === 'ZERO_RESULTS'){
        reject('Unable to find the address');
      }else if (body.status === 'OVER_QUERY_LIMIT') {
        reject(body.error_message);
      }else if (body.status === 'OK') {
        resolve({
          address: body.results[0].formatted_address,
          latitude: body.results[0].geometry.location.lat,
          longitude: body.results[0].geometry.location.lng
        });
      }else{
        reject('Request Failed');
      }
      // console.log(JSON.stringify(error, undefined, 2));
    });
  })
}

module.exports = {
  geocodeAddress
}
