const yargs = require('yargs');
const axios = require('axios');


const argv = yargs
  .options({
    address: {
      alias: 'a',
      demand: false,
      describe: 'Address to be search'
    }
  })
.help()
.alias('help','h')
.argv;

var encodedAddress = encodeURI(argv.address || 'Synerzip India');
var geocodeApiUrl = `http://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`;
const API_KEY = "ecf3d435c018a50b09ef3769f693023c";

axios.get(geocodeApiUrl).then((response) => {
  if(response.data.status === 'OK'){
    console.log(`Address: ${response.data.results[0].formatted_address}`);
    var longitude = response.data.results[0].geometry.location.lng;
    var latitude = response.data.results[0].geometry.location.lat;
    var weatherReportApiUrl = `https://api.darksky.net/forecast/${API_KEY}/${latitude},${longitude}`;
    return axios.get(weatherReportApiUrl);
  }else if(response.data.status === 'ZERO_RESULTS'){
    throw new Error('Unable to find the address');
  }else if(response.data.status === 'OVER_QUERY_LIMIT'){
    throw new Error(response.data.error_message);
  }
}).then((weatherResponse) => {
    var temperature = weatherResponse.data.currently.temperature;
    var apparentTemperature = weatherResponse.data.currently.apparentTemperature;
    var weatherSummary = weatherResponse.data.currently.summary;
    console.log(`It's currently ${temperature}. It's feel like ${apparentTemperature}. Weather will be '${weatherSummary}'`);
}).catch((error) => {
  if(error.code === 'ENOTFOUND'){
    console.log('Unable to connect');
  }else if(error.response && error.response.status === 403){
    console.log('Unable to connect');
  }else{
    console.log(error.message);
  }
});
