const yargs = require('yargs');
const geocode = require('./geocode/geocode');

const args = yargs
  .options({
    address:{
      describe: 'Address to fetch weather',
      demand: true,
      alias: 'a',
      string: true
    }
  })
  .help()
  .alias('help','h')
  .argv;
geocode.geocodeAddress(args.address,(errorMessage,result) => {
  if(errorMessage){
    console.log(errorMessage);
  }else{
    console.log(JSON.stringify(result,undefined, 2));
  }
});
